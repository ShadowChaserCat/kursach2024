import React from 'react';
import {Route, Routes} from "react-router-dom";
import IndexPage from "./pages/IndexPage";
import RegisterPage from "./pages/auth/RegisterPage";
import LoginPage from "./pages/auth/LoginPage";
import ErrorPage from "./pages/ErrorPage";


function App() {

    return (
        <div className="main-container min-h-screen">
            <main >
                <Routes>
                    <Route path="*" element={<ErrorPage isAuthenticated={false}/>}/>
                    <Route path="/" element={<IndexPage/>}/>
                    <Route path="/register" element={<RegisterPage/>}/>
                    <Route path="/login" element={<LoginPage/>}/>
                </Routes>
            </main>
        </div>
    );
}

export default App;
