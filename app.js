const express = require('express');
const config = require('config');
const mongoose = require('mongoose').default;

const app = express();
const PORT = config.get('port') || 5000;

app.use('/api/auth', require('./routes/auth.routes'));

async function startBD()  {
    try {
        console.log('hello');
        await mongoose.connect(config.get('mongoUri'));
    } catch (e) {
        console.log('Server error ' + e.message);
        process.exit(1);
    }
}



app.get('/', (req, res) => {
    res.send('hello server');
    startBD();
})

app.listen(PORT, () => {
    console.log(`App has been started, PORT = ${PORT}`);
})