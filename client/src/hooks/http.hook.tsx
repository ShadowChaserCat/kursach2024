import {useState, useCallback} from "react";

export interface IhttpRequest {
    url: string,
    method: string,
    body: null | BodyInit,
    headers: HeadersInit | undefined,
}

export const useHttp = () => {
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null)
    const request = useCallback(async ({url, method = 'GET', body = null, headers = {} }:IhttpRequest) => {
        setLoading(true)
        try {
            const response = await fetch(url, {method, body, headers} );
            const data = await response.json();

            if (!response.ok) {
                throw new Error('Fetch error' || data.message)
            }

            setLoading(false)
            return data;
        } catch (e:any) {
            setLoading(false);
            setError(e.message)
        }
    }, [])

    const clearError = () => {setError(null)}

    return {loading, request, error}
}