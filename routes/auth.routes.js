const express = require('express');
const User = require('../models/User');
const bcript = require('bcrypt');
const jwt = require('jsonwebtoken');
const {check, validationResult} = require('express-validator')
const router = express.Router();
const config = require('config');

// /api/auth/register
router.post(
    '/register',
    [
        check('email', 'Некорректный email').isEmail(),
        check('password', 'Некорректный password').isLength({min: 6})
    ]
    ,
    async (req, res) => {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({
                    error: errors.array(),
                    message: 'Некорректные данные при регистрации'
                })
            }
            const {email, password} = req.body;

            const candidate = await User.findOne({email})

            if (candidate) {
                res.status(400).json({message: 'Такой пользователь существует'})
            }

            const hashedPassword = await bcript.hash(password, 12);
            const user = new User({email, password: hashedPassword});
            await user.save();

            res.status(200).json({message: 'Пользователь создан!'})

        }
        catch (e) {
            res.status(500).json({message: 'Что-то пошло не так, попробуйте снова'})
        }
    })

// /api/auth/login
router.post(
    '/login',
    [
        check('email', 'Некорректный email').normalizeEmail().isEmail(),
        check('password', 'Некорректный password').exists()
    ]
    ,
    async (req, res) => {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({
                    error: errors.array(),
                    message: 'Некорректные данные при входе'
                })
            }
            const {email, password} = req.body

            const user = await User.findOne({ email })

            if (!user) {
                return res.status(400).json({message: 'Пользователь не найден'})
            }

            const isMatch = await bcript.compare(password, user.password)

            if (!isMatch) {
                return res.status(400).json({message: 'Неверный пароль, попробуйте снова'})
            }

            const token = jwt.sign(
                {userId: user.id},
                config.get('jwtSecret'),
                {expiresIn: '1h'}
            )

            res.json({token, userId: user.id})



        }
        catch (e) {
            res.status(500).json({message: 'Что-то пошло не так, попробуйте снова'})
        }
    })

module.exports = router;
