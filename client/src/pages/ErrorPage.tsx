import React, {useState} from "react";
import {Navigate, redirect} from "react-router-dom";

interface auth {
    isAuthenticated:boolean,

}

const ErrorPage = ({isAuthenticated}:auth) => {
    const [time, setTime] = useState(5)
    if (time > 0) {
        setTimeout(() => {
            setTime((state) => state - 1)
        }, 1000)
    }
    return (
        <div>
            Error
            <>
                <p>Вас перенаправит на главную страницу через {time} секунд</p>
                {isAuthenticated?  <p>Not auth</p> : ''}
                {time === 0 ? <Navigate to="/" /> : ''}
            </>

        </div>
    );
};

export default ErrorPage;