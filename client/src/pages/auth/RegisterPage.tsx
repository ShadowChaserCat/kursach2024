import React, {FormEvent, useEffect, useState} from 'react';
import {useHttp} from "../../hooks/http.hook";

const RegisterPage = () => {
    const {loading, error, request} = useHttp();
    const [formData, setFormData] = useState<BodyInit | null>(null)

    useEffect( () => {
        if (formData !== null) {
            registerHandler()
        }
    }, [formData])

    const submitHandler = async (event:FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        setFormData(new FormData(event.currentTarget))
    }

    const registerHandler = async () => {
        try {
            const data = await request( {url:'/api/auth/register', method:'POST', body:formData , headers:undefined})
            console.log('Data: ' + data)
        } catch (e:any) {

        }
    }

    return (
        <div>
            <h1>Логин</h1>
            <form onSubmit={(event) => {submitHandler(event)}}>
                <label className="flex gap-4 ml-4">
                    <input className="border" type={"email"} name="email" />
                    <input className="border" type={"text"} name="text" />
                </label>

                <button className="border">Отправить</button>
            </form>
        </div>
    );
};

export default RegisterPage;