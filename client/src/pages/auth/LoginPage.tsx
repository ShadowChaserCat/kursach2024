import React, {FormEvent, useState} from 'react';
import {useHttp} from "../../hooks/http.hook";

const LoginPage = () => {
    const {loading, error, request} = useHttp();

    const submitHandler = (event:FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        new FormData(event.currentTarget);
    }

    return (
        <div>
            <h1>Логин</h1>
            <form onSubmit={(event) => {submitHandler(event)}}>
                <label className="flex gap-4 ml-4">
                    <input className="border" type={"email"} name="email" />
                    <input className="border" type={"text"} name="text" />
                </label>

                <button className="border">Отправить</button>
            </form>
        </div>
    );
};

export default LoginPage;